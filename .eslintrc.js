module.exports = {
    'parserOptions': {
        'ecmaVersion': 8,
        'ecmaFeatures': {
            'experimentalObjectRestSpread': true,
        }
    },
    'env': {
        'browser': true,
        'es6': true,
        'node': true,
    },
    'rules': {
        "prefer-destructuring": "warn",
        "no-shadow": "warn",
        "keyword-spacing": "warn",
        "no-return-assign": "off",
        "object-shorthand": "warn",
        "operator-linebreak": ["error", "after"],
        "object-curly-spacing": ["warn", "never"],
        "prefer-const": "warn",
        "space-unary-ops": ["warn", {"words": true, "nonwords": false}],
        "arrow-parens": ["warn", "as-needed"],
        "max-len": ["warn", 160],
        'indent': ['warn', 4, { 'SwitchCase': 1 }],
        'semi': ['error', 'never'],
        'no-console': ['warn', { 'allow': ['warn', 'error'] }],
        'no-unexpected-multiline': 'warn',
        'comma-dangle': ['warn', 'always-multiline'],
        'no-unused-vars': 'error',
        'quote-props': ['error', 'as-needed'],
        'strict': 'off',
        'eol-last': 'off',
        'eqeqeq': ['error', 'smart'],
        'linebreak-style': 'off',
        'no-case-declarations': 'warn',
        'no-useless-escape': 'off',
        'default-case': 'error',
        'no-eval': 'error',
        'no-with': 'error',
        'newline-before-return': 'error',
        'quotes': ['error', 'single', { 'allowTemplateLiterals': true, 'avoidEscape': true }],
        'handle-callback-err': 'off',
        'space-before-function-paren': ['error', {'anonymous': 'never', 'named': 'never', 'asyncArrow': 'always'}],
        'no-undef': 'off'
    }
}
