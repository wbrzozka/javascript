const {transformArray} = require('../challenges/challenge06_reduce')

test('when input is invalid throw error', () => {
    const testData = [{input: undefined}, {input: []}]
    for (const data of testData) {
        expect(() => {transformArray(data.input)}).toThrowError(new Error('Błędne dane wejściowe'))
    }
})

test('when input is valid return correct object', () => {
    const testData = [
        {input: [['firstName', 'Bingo'], ['lastName', 'Szelest']], expected: {firstName: 'Bingo', lastName: 'Szelest'}},
        {input: [['price', 12.99], ['available', true], ['name', '']], expected: {price: 12.99, available: true, name: ''}},
    ]
    for (const data of testData) {
        expect(transformArray(data.input)).toEqual(data.expected)
    }
})