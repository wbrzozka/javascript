// za pomocą funkcji reduce przekształć tablicę dwuelementowych tablic ['fieldName', 'fieldValue'] w obiekt
// przykład
// input - [['firstName', 'Bingo'], ['lastName', 'Szelest']]
// output - {firstName: 'Bingo', lastName: 'Szelest'}
// jeżeli tablica wejściowa będzie pusta albo niezdefiniowana rzuć Errorem 'Błędne dane wejściowe', zakładamy, że w innych wypadkach tablica będzie poprawna
// potrzebna wiedza: spread operator, reduce, throw
// tipy
// przydać się też może część rozwiązania zadania 5
// Zamiast object.fieldName można też się odwołać do pola w obiekcie za pomocą object['fieldName'] i zadziała to tak samo

const transformArray = array => {
    // sprawdź poprawność tablicy i rzuć błedem jak trzeba

    // zrób odpowiedni reduce na tablicy i zwróc wynik

}

// od teraz testy są w osobnym pliku w folderze tests
module.exports.transformArray = transformArray

const transformArrayFor = array => {
    let result = {}
    for (const item of array) {
        console.log(result)
        result[item[0]] = item[1]
    }
    console.log(result)

    return result
}

transformArrayFor([['firstName', 'Bingo'], ['lastName', 'Szelest']])