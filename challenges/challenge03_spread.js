// za pomocą operatora spread połącz dwie tablice
// jeżeli któraś z tablic nie zostanie przekazana lub jest pusta, zwróć pustą tablicę ([])
// potrzebna wiedza: spread operator, truthy/falsy, operator logiczny and/or

const checkResult = require('../utils/checkResult')

const joinArrays = (array1, array2) => {
    // TODO
    // sprawdź czy tablice są niezdefiniowane lub puste, jeżeli tak, zwróć pustą tablicę, może się przydać wiedza o truthy i falsy

    // zwróć tablicę, która jest wynikiem połączenia tablic array1 i array2
    return []
}

let result = joinArrays([1,2],[3,4,5])
checkResult.checkResultArrays([1,2,3,4,5], result)
result = joinArrays([1,2])
checkResult.checkResultArrays([], result)
result = joinArrays([1,2], [])
checkResult.checkResultArrays([], result)
result = joinArrays(null, [3,4,5])
checkResult.checkResultArrays([], result)
result = joinArrays([3,4,5], [3,4,5])
checkResult.checkResultArrays([3,4,5,3,4,5], result)