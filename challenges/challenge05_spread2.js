// za pomocą operatora spread połącz dwa obiekty
// jeżeli któryś z obiektów nie zostanie przekazany zwróć pusty obiekt ({})
// potrzebna wiedza: spread operator, truthy/falsy, operator logiczny and/or

const {checkResultObjects} = require('../utils/checkResult')

const joinObjects = (object1, object2) => {
    // TODO
    // sprawdź czy obiekty są niezdefiniowane, jeżeli tak, zwróć pusty obiekt

    // zwróć obiekt, która jest wynikiem połączenia obiektów object1 i object2
    return {}
}

let result = joinObjects({name: 'Bingo'}, {dog: true})
checkResultObjects({name: 'Bingo', dog: true}, result)
result = joinObjects({name: 'Bingo'})
checkResultObjects({}, result)
result = joinObjects(null, {name: 'Bingo'})
checkResultObjects({}, result)
result = joinObjects({name: 'Bingo'}, {})
checkResultObjects({name: 'Bingo'}, result)
result = joinObjects({a: 'a', b: 2, c: 'c'}, {d: 'd', e: 'e', f: 'f', g: 1})
checkResultObjects({a: 'a', b: 2, c: 'c', d: 'd', e: 'e', f: 'f', g: 1}, result)