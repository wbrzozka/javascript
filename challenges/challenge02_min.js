// znajdź najmniejszą liczbę w tabeli
// jeżeli tablica jest pusta, zwróć 0

const {checkResult} = require('../utils/checkResult')

const arrayMin = array => {
    // TODO
    // sprawdź czy tablica jest pusta, jeżeli tak, zwróć 0

    // stwórz zmienną, w której będzie aktualnie najmniejsza wartość, a inicjalnie będzie miała bardzo dużą wartość (maksymalną dla liczby w js)

    // w pętli sprawdzaj czy kolejne wartości są mniejsze od aktualnie najmniejszej

    // zwróć najmniejszą wartość
    return array[0]
}

let result = arrayMin([1,2,3,4,5])
checkResult(1, result)
result = arrayMin([6,5,4,3,2])
checkResult(2, result)
result = arrayMin([10,12,5,121,54,69])
checkResult(5, result)
result = arrayMin([-10,-12,-5,121,-54,-69])
checkResult(-69, result)
result = arrayMin([])
checkResult(0, result)