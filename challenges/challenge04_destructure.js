// funkcja printName przyjmuje obiekt zawierający pola firstName i lastName i powinna zwrócić string w formacie 'Imię - firstName, Nazwisko - lastName',
// skorzystaj z destrukturyzacji obiektu
// jeżeli obiekt, lub któreś z jego pól nie zostanie przekazane, zwróć komunikat 'Brakujące dane'
// potrzebna wiedza: destrukturyzacja, truthy/falsy, operator logiczny and/or, template strings

const {checkResult} = require('../utils/checkResult')

const printName = person => {
    // TODO
    // sprawdź czy person ma wartość, jeżeli nie ma, zwróć komunikat

    // korzystając z destrukturyzacji obiektu wyciągnij z obiektu person pola firstName i lastName

    // sprawdź czy pola firstName i lastName mają wartość, jeżeli nie, zwróć komunikat

    // zwróć stringa zbudowanego z firstName i lastName
    return 'Brakujące dane'
}

let result = printName({firstName: 'Jan', lastName: 'Kowalski'})
checkResult('Imię - Jan, Nazwisko - Kowalski', result)
result = printName()
checkResult('Brakujące dane', result)
result = printName({lastName: 'Kowalski'})
checkResult('Brakujące dane', result)
result = printName({firstName: 'Jan'})
checkResult('Brakujące dane', result)
result = printName({firstName: 'Bingo', lastName: 'Szelest'})
checkResult('Imię - Bingo, Nazwisko - Szelest', result)