const simpleArray = [1,3,7,5,2,9,12,-3]

{
    console.log(simpleArray)
    // usuwa 2 elementy zaczynając od elementu 3 i zwraca je
    console.log(simpleArray.splice(3, 2))
    // nie ma już tych elementów w tablicy
    console.log(simpleArray)
    // teraz nie usuwam nic, więc splice zwraca pustą tablicę ale za to dodaje elementy 5 i 2 po 3 elemencie tablicy
    console.log(simpleArray.splice(3, 0, 5, 2))
    // i znowu są
    console.log(simpleArray)
    // ten splice robi to samo co poprzednie 2 za jednym razem, czyli zwraca 2 usunięte elementy, ale też dodaje je z powrotem
    console.log(simpleArray.splice(3, 2, 5, 2))
    console.log(simpleArray)
}