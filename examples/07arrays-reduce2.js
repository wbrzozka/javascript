// teraz chcemy napisać funkcję, która będzie działać na tablicy w taki sposób, żeby do parzystych elementów tej tablicy dodać 1, a nieparzyste zignorować

// funkcja napisana za pomocą pętli for of
const addOneToEvenArrayElements = array => {
    const result = []
    for (const item of array) {
        if (item % 2 === 0) {
            result.push(item + 1)
        }
    }

    return result
}

// to samo, ale używając najpierw filter, a potem map
const addOneToEvenArrayElementsFilterMap = array => array.filter(item => !(item % 2)).map(item => item + 1)

// teraz reduce, teraz inicjalnym wynikiem reduce jest pusta tablica, do której dodajemy parzyste elementy tablicy array powiększone o 1
const addOneToEvenArrayElementsReduce = array => array.reduce((result, item) => (
    // sprawdzamy czy reszta z dzielenia elementu tablicy przez 2 jest truthy (różna od 0), jeżeli tak (liczba jest nieparzysta, nie modyfikujemy wyniku,
    // zwracany jest po prostu aktualny wynik, a jeżeli liczba jest parzysta zwracamy nową tablicę, do której za pomocą spread operatora
    // kopiujemy aktualny wynik i dodajemy do tej tablicy nowy element - powiększony o 1 aktualnie przetwarzany element tablicy array
    item % 2 ? result : [...result, item + 1]
), [])

const array = [1,2,3,4]
console.log(addOneToEvenArrayElements(array))
console.log(addOneToEvenArrayElementsFilterMap(array))
console.log(addOneToEvenArrayElementsReduce(array))