// do przykładów z kodem asynchronicznym posłużę się https://jsonplaceholder.typicode.com/posts przykładowym API udostępnionym
// przez kogoś. pod tym adresem znajduje się tablica jakichś postów w formacie JSON

// żeby pobrać to co znajduje się pod adresem url można skorzystać na przykład z biblioteki axios
// jest to nowa biblioteka w projekcie więc jak jeszcze tego nie zrobiłaś to uruchom komendę yarn w terminalu
const axios = require('axios')

// w pierwszym przykładzie korzystam z promise, po zakończeniu pobierania danych w funkcji get wykonywana jest funkcja przekazana
// jako parametr do funkcji then.
let szelest = 'szelest'
console.log('promise then')
axios.get('https://jsonplaceholder.typicode.com/posts')
    .then(response => {
        console.log('Ilość danych: ', response.data.length)
        console.log(response.data[0])
        szelest = response.data.length
    })

console.log(szelest)

// teraz specjalnie zrobiłem literówkę w adresie, żeby wyłapać błąd w catch(), to że teraz używam .message, a wcześniej .data
// wynika z tego jak axios formatuje dane
console.log('promise catch')
axios.get('https://jsonplaceholder.typicode.com/pots')
    .then(response => {
        console.log('SUKCES')
    })
    .catch(error => {
        console.log('BŁĄD')
        console.log(error.message)
    })

// te same 2 przykłady z użyciem async await
// żeby móc użyć await, muszę stworzyć funkcję, która przed parametrami jest oznaczona słowem async, mówimy dzięki temu javascriptowi,
// że ta funkcja działa asynchronicznie i zamiast normalnego wyniku zwraca promise, który trzeba odpowiednio obłużyć
const asyncGet = async url => {
    try {
        const response = await axios.get(url)
        console.log('Ilość danych: ', response.data.length)
        console.log(response.data[0])
    } catch (error) {
        console.log('BŁĄD')
        console.log(error.message)
    }
}

console.log('await then')
asyncGet('https://jsonplaceholder.typicode.com/posts')
console.log('await catch')
asyncGet('https://jsonplaceholder.typicode.com/pots')

const executeAsync = async () => {
    console.log('await then')
    await asyncGet('https://jsonplaceholder.typicode.com/posts')
    console.log('await catch')
    await asyncGet('https://jsonplaceholder.typicode.com/pots')
}

executeAsync()