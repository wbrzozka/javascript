const object = {
    firstName: 'Wojciech',
    lastName: 'Brzózka',
    age: 28,
    female: false,
}

{
    const keys = Object.keys(object)
    const values = Object.values(object)
    console.log('obiekt', object)
    console.log('klucze obiektu', keys)
    console.log('wartości obiektu', values)
    console.log()
    console.log('for in jest pętlą, która iteruje po polach obiektu')
    for (const fieldName in object) {
        console.log(fieldName)
        console.log(`${fieldName} - ${object[fieldName]}`)
    }

    console.log()
    console.log('taki sam efekt można uzyskać za pomocą for of, ale najpierw samemu trzeba wyciągnąć klucze z obiektu')
    for (const key of keys) {
        console.log(key)
        console.log(`${key} - ${object[key]}`)
    }

    console.log()
    console.log('for of jest pętlą, która iteruje po kolejnych elementach tablicy')
    for (const value of values) {
        console.log(value)
    }

    console.log()
    console.log('to samo forEach')
    keys.forEach(key => console.log(key))
}