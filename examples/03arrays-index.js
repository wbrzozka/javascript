const simpleArray = [1,3,5,2,7,9,12,-3]
const objectsArray = [
    {id: 1, name: 'Andrzej'},
    {id: 2, name: 'Stefan'},
    {id: 3, name: 'Bingo'},
    {id: 4, name: 'Szelest'},
    {id: 5, name: 'Aga'},
    {id: 6, name: 'Wojtek'},
]

const getItemWithIdAndRemove = (array, id) => {
    // szukam indexu elementu o podanym id w tablicy
    const index = array.findIndex(item => item.id === id)
    // jeżeli element nie został znaleziony findIndex zwraca -1
    if (index > -1) {
        // w przeciwnym wypadku robię splice na tablicy, który usuwa elementy zaczynając od podanego indexu
        // i zwraca je, czyli wynikiem będzie tablica o długości podanej jako drugi parametr
        const removedItems = array.splice(index, 1)
        // przykład pokazujący jak przypisać pierwszy element tablicy do zmiennej za pomocą przypisania destrukturyzującego
        // poniższą linijkę można by też zapisać jako:
        // const removedItem = removedItems[0]
        const [removedItem] = removedItems

        return removedItem
    }
}

{
    // kopiuję tablicę do nowej, bo nie chcę, żeby z tej oryginalnej coś zostało usunięte
    const objectsArrayCopy = [...objectsArray]
    console.log('nie ma obbiektu o id 7 więc zwraca undefined -  ', getItemWithIdAndRemove(objectsArrayCopy, 7))
    console.log()
    console.log('tablica bez zmian -  ', objectsArrayCopy)
    console.log()
    console.log('obiekt o id 2 to Stefan, więc został zwrócony -  ', getItemWithIdAndRemove(objectsArrayCopy, 2))
    console.log()
    console.log('i nie ma go już w tablicy -  ', objectsArrayCopy)
    console.log()

    console.log('-1 bo to nie jest ten sam obiekt, który jest w tablicy -  ', objectsArrayCopy.indexOf({id: 4, name: 'Szelest'}))
    console.log()
    const newStefan = {id: 7, name: 'Stefan'}
    objectsArrayCopy.push(newStefan)
    console.log('teraz działa, bo to ten sam obiekt -  ', objectsArrayCopy.indexOf(newStefan))
    console.log()
    console.log('teraz nie ma takiego problemu, bo to tablica intów -  ', simpleArray.indexOf(7))
}