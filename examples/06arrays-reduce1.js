// prosta funkcja, która liczy sumę elementów tablicy za pomocą pętli for of
const sumArray = array => {
    let sum = 0
    for (const item of array) {
        sum += item
    }

    return sum
}

// podobna funkcja, tylko że suma tablicy jest powiększona o 2
const sumArrayPlus2 = array => {
    let sum = 2
    for (const item of array) {
        sum += item
    }

    return sum
}

// te same funkcje napisane za pomocą reduce

// reduce ma 2 parametry, pierwszy jest funkcją, która wykona się na każdym elemencie tablicy.
// funkcja ta też ma 2 parametry - pierwszy to aktualny wynik działania funkcji, a drugi to aktualnie przetwarzany element tablicy.
// drugi parametr reduce to inicjalna wartość (czyli taka jaka bedzie przy pierwszym przebiegu (dla pierwszego elementu)) pierwszego parametru
// wyżej wspomnianej funkcji, u nas ten parametr ma nazwę result
const sumArrayReduce = array => array.reduce((result, item) => (result + item), 0)

// drugi parametr reduce (inicjalny wynik) ma wartość 2, więc suma zawsze będzie powiększona o 2
const sumArrayPlus2Reduce = array => array.reduce((result, item) => (result + item), 2)

const array = [1,2,3,4]
console.log(sumArray(array))
console.log(sumArrayPlus2(array))
console.log(sumArrayReduce(array))
console.log(sumArrayPlus2Reduce(array))