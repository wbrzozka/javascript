const array = [1, 2, 3, 4, 5]

{
    console.log('tablica wejściowa -  ', array)
    console.log('map zwraca nową tablicę, która zawiera liczby z tablicy wejściowej podniesione do kwadratu -  ', array.map(number => number * number))
    console.log('tablica wejściowa jest bez zmian -  ', array)
    // tutaj pojawia się drugi parametr w map - index, jest to index w tablicy przetwarzanego elementu
    console.log('map może też zwrócić bardziej złożoną tablicę niż ta wejściowa -  ', array.map((number, index) => ({index: index + 1, square: number * number})))
    // to samo można zapisać też jak poniżej, ale dla tak prostych obliczeń preferowany jest krótszy zapis
    array.map((number, index) => {
        return {index: index + 1, square: number * number}
    })
    // nic też nie stoi na przeszkodzie, żeby zrobić z tego funkcję
    const square = (number, index) => {
        return {index: index + 1, square: number * number}
    }
    array.map((number, index) => {
        return square(number, index)
    })
    // a teraz uwaga, ostatni zapis można uprościć do
    console.log()
    console.log('magia  ', array.map(square))
    // map ma też trzeci parametr - tablica, na której wywoływana jest ta funkcja, w powyższych przykładach nie ma sensu używanie go,
    // bo jest to po prostu nasza tablica array, ale różne funkcje na tablicach można ze sobą łączyć i wtedy może się przydać
    // poniższy przykład trochę z dupy, ale załóżmy, że chcemy podnieść do kwadratu tylko parzyste elementy tablicy i dodać do zwróconych obiektów
    // ilość takich elementów
    console.log()
    console.log(array.filter(item => !(item % 2)).map((number, index, arr) => ({index: index + 1, square: number * number, length: arr.length})))

    // oczywiście parametry index i tablica nie występują tylko w funkcji map, ale też na przykład w filter, find czy findIndex
}