const users = require('../data/users')
const todos = require('../data/todos')
const posts = require('../data/posts')
const comments = require('../data/comments')

const getUserTodos = userId => todos.filter(todo => todo.userId === userId)

const blabla = users.map(user => {
    const userTodos = getUserTodos(user.id)
    const completedTodos = userTodos.filter(todo => todo.completed)

    return {
        id: user.id,
        completedTodos: completedTodos.length,
        notCompletedTodos: userTodos.length - completedTodos.length,
    }
})

console.log(blabla)