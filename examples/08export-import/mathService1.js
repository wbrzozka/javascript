// funkcja dodaje 2 liczby
const add = (a, b) => (a + b)

// funkcja odejmuje drugą liczbę od pierwszej
const subtract = (a, b) => (a - b)

// żeby można było korzystać z wyżej zdefiniowanych funkcji poza tym plikiem, należy powiedzieć node.js żeby je wyeksportował.
// robi się to dodając funkcje, które chcemy wyoksportować do obiektu module.exports
module.exports.add = add
module.exports.subtract = subtract