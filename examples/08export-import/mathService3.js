// teraz żeby użyć funkcji z innego pliku, musimy je zaimportować tutaj. i znowu można to zrobić na parę sposobów, łączy je natomiast operator require
// require jest to funkcja, którą daje nam node.js i która wymaga podania jednego parametru - ścieżki do pliku, który chcemy zaimportować
// w zamian dostajemy obiekt module.exports z danego pliku, który możemy przypisać do zmiennej, albo za pomocą operatora
// destrukturyzującego rozbić od razu na wiele zmiennych.
// przykłady
const mathService = require('./mathService1')
const {multiply, divide} = require('./mathService2')

// i teraz w zależności od tego w jaki sposób zaimportowaliśmy plik, inaczej też będziemy używać tych rzeczy
console.log('4 + 2 = ', mathService.add(4, 2))
console.log('4 - 2 = ', mathService.subtract(4, 2))
console.log('4 * 2 = ', multiply(4, 2))
console.log('4 / 2 = ', divide(4, 2))
