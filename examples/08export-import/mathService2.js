// funkcja mnoży dwie liczby
const multiply = (a, b) => (a * b)

// funkcja dzieli pierszą liczbę przez drugą
const divide = (a, b) => (a / b)

// znowu musimy funkcje wyeksportować. jest jeszcze trochę inny sposób na to.
// jak już wspomniałem module.exports jest obiektem, więc możemy do niego przypisać od razu cały obiekt
// zamiast funkcja po funkcji
module.exports = {
    multiply,
    divide,
}