// parametry
// jeżeli funkcja ma 0 albo 2 lub więcej parametrów, obowiązkowe są nawiasy wokół parametrów
// () => {...}
// (a, b) => {...}
// jeżeli jeden parametr, nawiasów nie trzeba stosować i raczej się je pomija
// a => {...}

// return
// funkcja nie musi nic zwracać, wówczas ciało funkcji musi być wewnątrz nawiasów klamrowych

// ta funkcja nie robi nic więcej oprócz przypisania do pola firstName obiektu person wartości parametru name
const setFirstName = (person, name) => {
    person.firstName = name
}

// jeżeli funkcja coś zwraca i wykonywana jest wewnątrz jakaś inna logika też muszą być nawiasy klamrowe
const getName = person => {
    if (!person || !person.name) {
        return 'brak'
    }

    return person.name
}

// jeżeli ciało funkcji sprowadza się tylko do słowa return i tego co po nim, to całość też może być wewnątrz nawiasów klamrowych
// to samo co wyżej, ale zapisane w jednej linijce
const getName2 = person => {
    return person && person.name ? person.name : 'brak'
}

// ale w takim przypadku nawiasy klamrowe można pominąć i wtedy pomija się też return
const getName3 = person => person && person.name ? person.name : 'brak'

// można też to co po strzałce umieścić w nawiasach okrągłych
const getName4 = person => (person && person.name ? person.name : 'brak')

// jeszcze jeden przypadek - jeżeli funkcja składa się tylko z return, ale zwraca obiekt, tak jak poniżej
const getPerson = (firstName, lastName) => {
    return {firstName: firstName, lastName: lastName}
}

// to nie możemy zapisać tego jako
// const getPerson = (firstName, lastName) => {firstName: firstName, lastName: lastName}
// bo po nawiasie klamrowym otwierającym JS oczekuje funkcji w takiej sytuacji
// co nie znaczy, że nie możemy skrócić takiej funkcji.
// musimy po prostu obiekt otoczyć nawiasami okrągłymi, czyli:
const getPerson2 = (firstName, lastName) => ({firstName: firstName, lastName: lastName})

// ostatnia rzecz, jak pewnie zauważyłaś webstorm koloruje w powyższych przykładach firstName i lastName
// to dlatego, że gdy tworzymy obiekt i jedno lub więcej z pól tego obiektu bierze wartość ze zmiennej,
// która nazywa się tak samo jak chcemy, żeby nazywało się pole obiektu, to możemy pominąć nazwę pole, czyli getPerson2 może też wyglądać tak:
const getPerson3 = (firstName, lastName) => ({firstName, lastName})

// JS automatycznie nazwie pola obiektu tak jak nazywają się te zmienne
const person = getPerson3('Bingo', 'Szelest')
console.log(`firstName - ${person.firstName}, lastName - ${person.lastName}`)