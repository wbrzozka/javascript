const checkResult = (expected, actual) => {
    if (expected !== actual) {
        logError(expected, actual)
    }
}

const checkResultArrays = (expected, actual) => {
    if (expected.length !== actual.length || expected.some((value, index) => value !== actual[index])) {
        logError(expected, actual)
    }
}

const checkResultObjects = (expected, actual) => {
    if (JSON.stringify(expected) !== JSON.stringify(actual)) {
        logError(JSON.stringify(expected), JSON.stringify(actual))
    }
}

const logError = (expected, actual) => {
    console.error(`${expected} expected - ${actual} actual`)
    console.trace()
    console.log()
}

module.exports.checkResult = checkResult
module.exports.checkResultArrays = checkResultArrays
module.exports.checkResultObjects = checkResultObjects